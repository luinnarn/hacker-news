import React, { Component } from 'react';
import Comment from './Comment';
import { fetchItem } from './service/DataService';

class CommentsList extends Component {

    state = {
        comments: [],
        loaded:false
    };
    
    componentDidUpdate(prevProps, prevState) {
        const parentId = this.props.id;
        if (this.props.show && !this.state.loaded) {
            fetchItem(parentId)
                .then(result => {
                this.setState({comments: result.data.kids, loaded: true});
            });
        }
    }

    render() {
        const comments = this.state.comments.map((comment, i) => 
            <Comment key={i} id={comment} />
        );
        return(
            <div className="commentsList">
                {comments}
            </div>
        );
    }
}

export default CommentsList;
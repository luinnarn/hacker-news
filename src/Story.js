import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { fetchItem } from './service/DataService';

class Story extends Component {
    
    state = {
        loaded : false
    }

    componentDidMount() {
        fetchItem(this.props.id)
        .then( result => {
            this.setState({...result.data, loaded: true});
        })
    }

    render() {
        const {by, descendants, id, score, title, url, loaded} = this.state;
        const numberOfComments = descendants + ' ' + (descendants === 1 ? 'comment' : 'comments');
        return loaded && (
            <li key = {id}>
                <a href={url} target="blank">{title}</a>
                <div className="item-info">
                    <span>{score} {score && ' points '}</span>
                    <span> {by &&' by '} {by} </span>
                    <span>
                        <Link to={'/' + id}> 
                            {descendants > 0 && numberOfComments}
                        </Link>
                    </span>
                </div>
            </li>
        )
    }
}

export default Story;
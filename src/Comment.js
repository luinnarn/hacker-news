import React, { Component } from 'react';
import CommentsList from './CommentsList';
import { fetchItem } from './service/DataService';

class Comment extends Component {

    state = {
        kids: [],
        show: false
    }
    
    componentDidMount() {
        fetchItem(this.props.id)
        .then(result => {
            this.setState({...result.data});
        })
    }

    toggleShow = () => {
        const current = this.state.show;
        this.setState({ show: !current });
    }

    render() {
        const nestedCommentsList = (
            <div>
                <button className={this.state.show ? 'btn comment-accordion' : 'btn comment-accordion collapsed'} onClick={this.toggleShow}></button>
                <div id={this.props.id} className={this.state.show ? "collapse-show" : "collapse-hide"}>
                    <CommentsList id={this.props.id} show={this.state.show} />
                </div>
            </div>
        )
        return(
            <div className="comment">
                <p dangerouslySetInnerHTML = {{__html: this.state.text}}></p>
                <span className="item-info">by: {this.state.by}</span>
                {this.state.kids.length > 0 && nestedCommentsList}
            </div>
        );
    }
}

export default Comment;
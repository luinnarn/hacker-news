import React, { Component } from 'react';
import CommentsList from './CommentsList';
import Sidebar from './Sidebar';
import { fetchItem } from './service/DataService';

class CommentsWrapper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title : '',
            id : this.props.match.params.id
        }
    }

    componentDidMount() {
        fetchItem(this.state.id)
        .then(result => {
            this.setState({title: result.data.title});
        })
    }

    render() {
        return (
            <div id="top">
                <Sidebar />
                <h2 className="comment-title">{this.state.title} - comments</h2>
                <CommentsList id ={this.state.id} show={true} />
            </div>
        )}
}

export default CommentsWrapper;
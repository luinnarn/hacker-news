import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import logo from './computer.svg';
import './App.css';
import StoriesList from './StoriesList';
import CommentsWrapper from './CommentsWrapper';

class App extends Component {
  
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <Link to = "/">
              <img src={logo} className="App-logo" alt="logo" />
              <h1 className="App-title">Hacker News</h1>
            </Link>
          </header>
          <div className="container App-content">
            <main>
              <Route exact path = "/" component = {StoriesList} />
              <Route path = "/:id" component = {CommentsWrapper} />
            </main>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;

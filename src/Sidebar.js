import React, { Component } from 'react';
import StickyBox from "react-sticky-box";
import {  Link } from "react-router-dom";

class Sidebar extends Component {
    
    render() {
        return(
            <StickyBox className="sticky-sidebar">
                <ul>
                    <li><Link to = "/">Home</Link></li>
                    <li><a href="#top">Back to top</a></li>
                </ul>
            </StickyBox>
        )
    }
}

export default Sidebar
import React, { Component } from 'react';
import Story from './Story';
import Pagination from './Pagination';
import { fetchStories } from './service/DataService';


class StoriesList extends Component {

    state = {
        stories:[], 
        pageItems: [], 
        startItemIndex: 1, 
        loaded: false
    }

    componentDidMount() {
        fetchStories()
        .then(result => {
            this.setState({stories: result.data, loaded: true})
        })
    }

    onChangePage = (pageItems, startItemIndex) => {
        this.setState({ pageItems: pageItems, startItemIndex: startItemIndex });
    }

    render() {
        const stories = this.state.pageItems.map((story, i) => 
            <Story key={i} id={story}/>
        );
        return this.state.loaded && (
            <div className = "container">
                <Pagination items={this.state.stories} onChangePage={this.onChangePage} />
                <ol start={this.state.startItemIndex}>
                {stories}
                </ol>
            </div>
        )

    }
}

export default StoriesList;
import axios from 'axios';

export function fetchStories() {
    return new Promise ((resolve, reject) => {
        axios.get('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty')
        .then(result => resolve(result))
    }
)}

export function fetchItem(item) {
    return new Promise ((resolve, reject) => {
        axios.get(`https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`)
        .then(result => resolve(result))
    }
)}